import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  // J'initialise ma variable dans laquelle je viendrai stocker mes données
  characters: any = {};
  planets: any = {};
  vehicles: any = {};

  constructor(private router: Router, private httpClient: HttpClient) { }

  ngOnInit(): void {
    // J'appelle ma méthode ici pour qu'elle soit executée à l'initialisation du composant
    this.getCharacters();
    this.getPlanets();
    this.getVehicles();
  }

  // Cette méthode est appelée dans le template
  // Elle permet de créer une navigation à partir de la partie logique
  // Cette méthode est parfois utile 
  // Exemple : lorsque l'on souhaite orienter l'utilisateur vers un composant 
  // suite à sa connexion 
  redirection() {
    this.router.navigateByUrl('planets');
  }

  // Rechercher les données sur la base de donnée : les personnages
  // Méthode appelée dans le ngOnInit()
  getCharacters() {
    // Fournir à la methode get() l'url ciblée (qui permet d'aller rechercher la donnée)
    // .subscribe() -> je souhaite faire quelque chose des données reçues
    //      ex: les stocker dans une variable pour les afficher dans le template
    this.httpClient.get("https://swapi.dev/api/people").subscribe(peoples => {
      this.characters = peoples;
    })
  }

  // Rechercher les données sur la base de donnée : les planètes
   // Méthode appelée dans le ngOnInit()
  getPlanets() {
    // Fournir à la methode get() l'url ciblée (qui permet d'aller rechercher la donnée)
    // .subscribe() -> je souhaite faire quelque chose des données reçues
    //      ex: les stocker dans une variable pour les afficher dans le template
    this.httpClient.get("https://swapi.dev/api/planets/").subscribe(planets => {
      this.planets = planets;
    })
  }

  // Rechercher les données sur la base de donnée : les vehicules
   // Méthode appelée dans le ngOnInit()
  getVehicles() {
    // Fournir à la methode get() l'url ciblée (qui permet d'aller rechercher la donnée)
    // .subscribe() -> je souhaite faire quelque chose des données reçues
    //      ex: les stocker dans une variable pour les afficher dans le template
    this.httpClient.get("https://swapi.dev/api/vehicles/").subscribe(vehicles => {
      this.vehicles = vehicles;
    })
  }
}
