import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-index-planets',
  templateUrl: './index-planets.component.html',
  styleUrls: ['./index-planets.component.scss']
})
export class IndexPlanetsComponent implements OnInit {
  @Output() sendPlanetToParent = new EventEmitter();
  // J'initialise ma variable à null 
  // Lorsque l'on recevra les données, nous les ajouterons à celle-ci
  planets: any = null;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    // J'appelle ma méthode ici pour qu'elle soit executée à l'initialisation du composant
    this.getPlanets();
  }

  // Rechercher les données sur la base de donnée : les planètes
  getPlanets() {
    this.planets = [];
    // Boucle qui nous permet de parcourir les différentes pages de l'API
    for (let index = 1; index <= 6; index++) {
      // Fournir à la methode get() l'url ciblée (qui permet d'aller rechercher la donnée)
      // .subscribe() -> je souhaite faire quelque chose des données reçues
      //      ex: les stocker dans une variable pour les afficher dans le template
      this.httpClient.get("https://swapi.dev/api/planets/?page=" + index).subscribe((pageOfPlanets: any) => {
        // Comment attribuer les planets à mon tableau 
        // .push() ou .concat()
        this.planets = this.planets.concat(pageOfPlanets.results);
      })
    }
  }

  // 
  showPlanet(planet: any) {
    this.sendPlanetToParent.emit(planet);
  }
}
